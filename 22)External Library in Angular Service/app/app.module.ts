import {NgModule} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'
import {CoursesAppComponent} from './courses-app.component'
import {CoursesListComponent} from './courses/courses-list.component'
import {CoursesSingleComponent} from './courses/courses-single.component'
import {HeaderComponent} from './utility/header.component'
import { CourseService } from  './courses/shared/course.service'
import { ToastrService } from  './common/shared/toastr.service'

@NgModule({
    imports: [BrowserModule],
    declarations: [CoursesAppComponent, 
                   CoursesListComponent, 
                   CoursesSingleComponent,
                   HeaderComponent],
    providers: [CourseService, ToastrService],
    bootstrap: [CoursesAppComponent]
})

export class AppModule {}

