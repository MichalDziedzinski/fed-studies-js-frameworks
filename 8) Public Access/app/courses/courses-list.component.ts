import { Component } from '@angular/core'

@Component({
    selector: 'courses-list',
    template: `
    <h1> Courses </h1>
    <h2>by: {{firstCourse.customMessage}}</h2>
    <courses-single #firstCourse [course]="course" 
        (clickEvent)="handleCourseClickEvent($event)"></courses-single>
    <button class="btn btn-primary" (click)="firstCourse.logCourseAuthor()">
        Log author Name </button>
    `
})

export class CoursesListComponent {
    course = {
        title: 'Js Frameworks',
        author: {
            firstName: 'Karol',
            lastName: 'Rogowski'
        } ,
        price: 1.00
    }

    handleCourseClickEvent(event){
        console.log(event + " Parent level console log")
    }
}



