import { Component } from '@angular/core'

@Component({
    selector: 'courses-app',
    template: '<header></header><courses-list></courses-list>'
})

export class CoursesAppComponent {

}

