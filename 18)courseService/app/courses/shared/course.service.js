"use strict";
var DifLevel_1 = require("../../utility/enums/DifLevel");
var CourseService = (function () {
    function CourseService() {
    }
    CourseService.prototype.getCourses = function () {
        return COURSES;
    };
    return CourseService;
}());
exports.CourseService = CourseService;
var COURSES = [{
        id: 1,
        active: true,
        level: DifLevel_1.DifLevel.Begginer,
        title: 'Js Frameworks',
        author: {
            firstName: 'Karol',
            lastName: 'Rogowski'
        },
        price: 1.00
    }, {
        id: 2,
        active: false,
        level: DifLevel_1.DifLevel.Begginer,
        title: 'Type Script',
        author: null,
        price: 3.00
    },
    {
        id: 3,
        active: true,
        level: DifLevel_1.DifLevel.Expert,
        title: 'Power Shell',
        author: {
            firstName: 'Karol',
            lastName: 'Rogowski'
        },
        price: 2.00
    }];
//# sourceMappingURL=course.service.js.map