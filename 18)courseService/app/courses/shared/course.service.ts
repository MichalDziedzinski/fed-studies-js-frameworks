import { DifLevel } from '../../utility/enums/DifLevel'

export class CourseService{
    getCourses(){
        return COURSES
    }
}

const COURSES = [{
    id:1,
    active: true,
    level: DifLevel.Begginer,
    title: 'Js Frameworks',
    author: {
        firstName: 'Karol',
        lastName: 'Rogowski'
    } ,
    price: 1.00
}, {
    id:2,
    active: false,
    level: DifLevel.Begginer,
    title: 'Type Script',
    author: null ,
    price: 3.00
},
{
    id:3,
    active: true,
    level: DifLevel.Expert,
    title: 'Power Shell',
    author: {
        firstName: 'Karol',
        lastName: 'Rogowski'
    } ,
    price: 2.00
}]