import { Component } from '@angular/core'

@Component({
    selector: 'courses-app',
    template: `<header></header>
               <router-outlet></router-outlet>`
})

export class CoursesAppComponent {

}

