//courses/1
import {Component} from '@angular/core'
import { CourseService } from './shared/course.service'
import { DifLevel } from '../utility/enums/DifLevel'

@Component({
    templateUrl: 'app/courses/course-details.component.html'
})

export class CourseDetailsComponent{
    DifLevel: typeof DifLevel = DifLevel;
    course: any;
    
    constructor (private courseService: CourseService){      
    }

    ngOnInit(){
        this.course = this.courseService.getCourse(2);
    }
}