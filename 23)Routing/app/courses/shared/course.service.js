"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var DifLevel_1 = require("../../utility/enums/DifLevel");
var core_1 = require("@angular/core");
var CourseService = (function () {
    function CourseService() {
    }
    CourseService.prototype.getCourses = function () {
        return COURSES;
    };
    CourseService.prototype.getCourse = function (id) {
        return COURSES.find(function (c) { return c.id === id; });
    };
    return CourseService;
}());
CourseService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], CourseService);
exports.CourseService = CourseService;
var COURSES = [{
        id: 1,
        active: true,
        level: DifLevel_1.DifLevel.Begginer,
        title: 'Js Frameworks',
        author: {
            firstName: 'Karol',
            lastName: 'Rogowski'
        },
        price: 1.00
    }, {
        id: 2,
        active: false,
        level: DifLevel_1.DifLevel.Begginer,
        title: 'Type Script',
        author: null,
        price: 3.00
    },
    {
        id: 3,
        active: true,
        level: DifLevel_1.DifLevel.Expert,
        title: 'Power Shell',
        author: {
            firstName: 'Karol',
            lastName: 'Rogowski'
        },
        price: 2.00
    }];
//# sourceMappingURL=course.service.js.map