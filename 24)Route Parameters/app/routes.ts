import {Routes} from '@angular/router'
import {CoursesListComponent} from './courses/courses-list.component'
import {CourseDetailsComponent} from './courses/course-details.component'

export const appRoutes:Routes = [
    {
        path:'courses',
        component:CoursesListComponent
    },
    {
        path:'courses/:id',
        component:CourseDetailsComponent
    },
    {
        path: '',
        redirectTo: '/courses',
        pathMatch: 'full'
    }
]
