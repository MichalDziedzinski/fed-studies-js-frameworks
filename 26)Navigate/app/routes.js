"use strict";
var courses_list_component_1 = require("./courses/courses-list.component");
var course_details_component_1 = require("./courses/course-details.component");
var course_new_component_1 = require("./courses/course-new.component");
exports.appRoutes = [
    {
        path: 'courses/new',
        component: course_new_component_1.CourseNewComponent
    },
    {
        path: 'courses',
        component: courses_list_component_1.CoursesListComponent
    },
    {
        path: 'courses/:id',
        component: course_details_component_1.CourseDetailsComponent
    },
    {
        path: '',
        redirectTo: '/courses',
        pathMatch: 'full'
    }
];
//# sourceMappingURL=routes.js.map