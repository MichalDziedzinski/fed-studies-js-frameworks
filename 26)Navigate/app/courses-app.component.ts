import { Component } from '@angular/core'
import { Router } from '@angular/router'

@Component({
    selector: 'courses-app',
    template: `<header></header>
               <router-outlet></router-outlet>
               <button type="button" class="btn btn-success" 
               (click)="createNew()">CREATE NEW</button>`
})

export class CoursesAppComponent {
    constructor(private router:Router){
        
    }

    createNew(){
        this.router.navigate(['/courses/new'])
    }
}

