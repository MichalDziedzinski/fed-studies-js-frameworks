import { Component, Input, Output, EventEmitter } from '@angular/core'
import { DifLevel } from '../utility/enums/DifLevel'

@Component({
    selector: 'courses-single',
    templateUrl: 'app/courses/courses-single.component.html'
})

export class CoursesSingleComponent {
    DifLevel: typeof DifLevel = DifLevel;

    @Input() course:any
    @Output() clickEvent = new EventEmitter();

    customMessage:string

    clickMe(){
        console.log(this.course.title)
        this.clickEvent.emit(this.course.title + " clicked")
    }
}



