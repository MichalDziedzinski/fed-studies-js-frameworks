import {Routes} from '@angular/router'
import {CoursesListComponent} from './courses/courses-list.component'
import {CourseDetailsComponent} from './courses/course-details.component'
import { CourseNewComponent } from './courses/course-new.component';

export const appRoutes:Routes = [
    {//ORDER !!!!!
        path:'courses/new',
        component:CourseNewComponent
    },
    {
        path:'courses',
        component:CoursesListComponent
    },
    {
        path:'courses/:id',
        component:CourseDetailsComponent
    },
    {
        path: '',
        redirectTo: '/courses',
        pathMatch: 'full'
    }
]
