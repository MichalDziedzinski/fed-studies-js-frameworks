import { Component, Input, Output, EventEmitter } from '@angular/core'

@Component({
    selector: 'courses-single',
    templateUrl: 'app/courses/courses-single.component.html'
})

export class CoursesSingleComponent {
    @Input() course:any
    @Output() clickEvent = new EventEmitter();

    customMessage:string

    ngOnInit(){
        this.customMessage = this.course.author.firstName + " " + this.course.author.lastName
    }
    
    clickMe(){
        console.log(this.course.title)
        this.clickEvent.emit(this.course.title + " clicked")
    }

    logCourseAuthor(){
        console.log(this.course.author.firstName + " " + this.course.author.lastName)
    }
}



