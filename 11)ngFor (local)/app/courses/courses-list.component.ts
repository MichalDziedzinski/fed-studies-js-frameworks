import { Component } from '@angular/core'

@Component({
    selector: 'courses-list',
    template: `
    <h1> Courses </h1>
    <div class="row">
        <div *ngFor="let course of courses" class="col-md-5">
            <courses-single [course]="course" 
            (clickEvent)="handleCourseClickEvent($event)"></courses-single>
        </div>
    </div>
    `
})

export class CoursesListComponent {
    courses = [{
        id:1,
        title: 'Js Frameworks',
        author: {
            firstName: 'Karol',
            lastName: 'Rogowski'
        } ,
        price: 1.00
    }, {
        id:2,
        title: 'Type Script',
        author: {
            firstName: 'Karol',
            lastName: 'Rogowski'
        } ,
        price: 3.00
    },
    {
        id:3,
        title: 'Power Shell',
        author: {
            firstName: 'Karol',
            lastName: 'Rogowski'
        } ,
        price: 2.00
    }]

    handleCourseClickEvent(event){
        console.log(event + " Parent level console log")
    }
}



