//courses/1
import {Component} from '@angular/core'
import { CourseService } from './shared/course.service'
import { DifLevel } from '../utility/enums/DifLevel'
import { ActivatedRoute } from '@angular/router'

@Component({
    templateUrl: 'app/courses/course-details.component.html'
})

export class CourseDetailsComponent{
    DifLevel: typeof DifLevel = DifLevel;
    course: any;
    
    constructor (private courseService: CourseService,
                private route: ActivatedRoute){      
    }

    ngOnInit(){      
        this.course = this.courseService.getCourse(
            +this.route.snapshot.params['id']
        );
    }
}