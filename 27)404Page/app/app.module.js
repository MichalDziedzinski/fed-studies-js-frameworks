"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var courses_app_component_1 = require("./courses-app.component");
var courses_list_component_1 = require("./courses/courses-list.component");
var courses_single_component_1 = require("./courses/courses-single.component");
var header_component_1 = require("./utility/header.component");
var course_service_1 = require("./courses/shared/course.service");
var toastr_service_1 = require("./common/shared/toastr.service");
var course_details_component_1 = require("./courses/course-details.component");
var router_1 = require("@angular/router");
var routes_1 = require("./routes");
var course_new_component_1 = require("./courses/course-new.component");
var Error404_component_1 = require("./errors/Error404.component");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            router_1.RouterModule.forRoot(routes_1.appRoutes, { useHash: true })
        ],
        declarations: [courses_app_component_1.CoursesAppComponent,
            courses_list_component_1.CoursesListComponent,
            courses_single_component_1.CoursesSingleComponent,
            header_component_1.HeaderComponent,
            course_details_component_1.CourseDetailsComponent,
            course_new_component_1.CourseNewComponent,
            Error404_component_1.Error404Component],
        providers: [course_service_1.CourseService, toastr_service_1.ToastrService],
        bootstrap: [courses_app_component_1.CoursesAppComponent]
    }),
    __metadata("design:paramtypes", [])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map