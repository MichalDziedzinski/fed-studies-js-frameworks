import { Component } from '@angular/core'

@Component({
    selector: 'courses-list',
    templateUrl: 'app/courses/courses-list.component.html'
})

export class CoursesListComponent {
    course = {
        title: 'Js Frameworks',
        author: {
            firstName: 'Karol',
            lastName: 'Rogowski'
        } ,
        price: 1.00
    }
}



