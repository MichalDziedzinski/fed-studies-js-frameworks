import { Component } from '@angular/core'

@Component({
    selector: 'courses-list',
    template: `
        <div>
            <h1> Best Courses </h1>
            <hr>
            <div>{{course.title}}</div>
            <div>Author: {{course.author.firstName}} {{course.author.lastName}}</div>
            <div>Price \${{course.price}}</div>
        </div>
    `
})

export class CoursesListComponent {
    course = {
        title: 'Js Frameworks',
        author: {
            firstName: 'Karol',
            lastName: 'Rogowski'
        } ,
        price: 1.00
    }
}



