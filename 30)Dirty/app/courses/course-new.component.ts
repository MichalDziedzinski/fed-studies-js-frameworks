import { Component } from '@angular/core'
import { Router } from '@angular/router'

@Component({
    template: `<h1>New Component</h1>
    <hr/>
    <div>
        <h3>INPUTS :) </h3>
        <br/>
        <button type="submit" class="btn btn-primary">SAVE</button>
        <button type="button" class="btn btn-default" (click)="cancel()">CANCEL</button>
    </div>
    `
})

export class CourseNewComponent {
    isDirty:boolean = true;

    constructor(private router:Router){    
    }

    cancel(){
        this.router.navigate(['/courses'])
    }
}