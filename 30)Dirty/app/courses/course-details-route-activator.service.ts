import {Router, ActivatedRouteSnapshot, 
        CanActivate} from "@angular/router"
import { Injectable } from "@angular/core"
import { CourseService } from "./shared/course.service"

@Injectable()
export class CourseDetailsRouteActivator implements CanActivate {  
    constructor(private courseService: CourseService,
                private router: Router){}

    canActivate(route: ActivatedRouteSnapshot){
        const id = +route.params['id']
        const exists = !!this.courseService.getCourse(id)
        if(!exists) this.router.navigate(['/404'])
        return exists
    }
}