import { Component } from '@angular/core'
import { CourseService } from './shared/course.service'

@Component({
    selector: 'courses-list',
    template: `
    <h1> Courses </h1>
    <div class="row">
        <div *ngFor="let course of courses" 
              [hidden]="!course.active" class="col-md-5">
            <courses-single [course]="course" 
            (clickEvent)="handleCourseClickEvent($event)"></courses-single>
        </div>
    </div>
    `
})

export class CoursesListComponent {
    courses: any;
    
    constructor (private courseService: CourseService){
        this.courses = courseService.getCourses();
    }

    handleCourseClickEvent(event){
        console.log(event + " Parent level console log")
    }
}



