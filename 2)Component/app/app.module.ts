import {NgModule} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'
import {CoursesAppComponent} from './courses-app.component'

@NgModule({
    imports: [BrowserModule],
    declarations: [CoursesAppComponent],
    bootstrap: [CoursesAppComponent]
})

export class AppModule {}

