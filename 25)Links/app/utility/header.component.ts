import { Component } from '@angular/core'

@Component({
    selector: 'header',
    template: `<h1 [routerLink]="['/courses']" class="well hoverwell">PAGE HEADER</h1>
    <hr/>
    `
})

export class HeaderComponent {
}



