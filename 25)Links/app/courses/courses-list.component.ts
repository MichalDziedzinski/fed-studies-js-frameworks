import { Component } from '@angular/core'
import { CourseService } from './shared/course.service'
import { ToastrService } from '../common/shared/toastr.service'

@Component({
    template: `
    <h1> Courses </h1>
    <div class="row">
        <div *ngFor="let course of courses" 
              [hidden]="!course.active" class="col-md-5">
            <courses-single [course]="course" 
            (clickEvent)="handleCourseClickEvent($event)"></courses-single>
        </div>
    </div>
    `
})

export class CoursesListComponent {
    courses: any;
    
    constructor (private courseService: CourseService,
                private toastrService:ToastrService){      
    }

    ngOnInit(){
        this.courses = this.courseService.getCourses();
    }

    handleCourseClickEvent(event){
        this.toastrService.info(event + " was clicked");
    }
}



