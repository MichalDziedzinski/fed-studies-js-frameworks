import { Component } from '@angular/core'

@Component({
    selector: 'courses-list',
    template: `
    <h1> Courses </h1>
    <courses-single [course]="course" 
        (clickEvent)="handleCourseClickEvent($event)"></courses-single>
    `
})

export class CoursesListComponent {
    course = {
        title: 'Js Frameworks',
        author: {
            firstName: 'Karol',
            lastName: 'Rogowski'
        } ,
        price: 1.00
    }

    handleCourseClickEvent(event){
        console.log(event + " Parent level console log")
    }
}



