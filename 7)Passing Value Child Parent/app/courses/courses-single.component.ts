import { Component, Input, Output, EventEmitter } from '@angular/core'

@Component({
    selector: 'courses-single',
    templateUrl: 'app/courses/courses-single.component.html'
})

export class CoursesSingleComponent {
    @Input() course:any
    @Output() clickEvent = new EventEmitter();

    clickMe(){
        console.log(this.course.title)
        this.clickEvent.emit(this.course.title + " clicked")
    }
}



