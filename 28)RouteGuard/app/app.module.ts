import {NgModule} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'
import {CoursesAppComponent} from './courses-app.component'
import {CoursesListComponent} from './courses/courses-list.component'
import {CoursesSingleComponent} from './courses/courses-single.component'
import {HeaderComponent} from './utility/header.component'
import { CourseService } from  './courses/shared/course.service'
import { ToastrService } from  './common/shared/toastr.service'
import { CourseDetailsComponent} from './courses/course-details.component'
import { RouterModule } from '@angular/router'
import { appRoutes } from './routes'
import { CourseNewComponent } from './courses/course-new.component'
import { Error404Component } from './errors/Error404.component'
import { CourseDetailsRouteActivator } 
from './courses/course-details-route-activator.service'


@NgModule({
    imports: 
    [
        BrowserModule,
        RouterModule.forRoot(appRoutes, { useHash: true })
    ],
    declarations: [CoursesAppComponent, 
                   CoursesListComponent, 
                   CoursesSingleComponent,
                   HeaderComponent,
                   CourseDetailsComponent,
                   CourseNewComponent,
                   Error404Component],
    providers: [CourseService, ToastrService,
         CourseDetailsRouteActivator],
    bootstrap: [CoursesAppComponent]
})

export class AppModule {}

